#Vendor Code Repository Requirements and Expectations

##Deloitte Experience Delivery Platforms Team will:
1. Provide vendor with read-access to D.com HTML Templates repository per [Deloitte Custom HTML Requirements](http://ww2.deloitte.com).
2. Create a project/product specific repository for vendor development work, establish correct HTML and static assets folder hierarchy for vendor development team to follow, and provide write-access to necessary vendor development team members.
3. Deploy vendor commits to AEM in a timely manner per team needs for testing and delivery.
4. Ensure any code updates impacting project/product not developed by vendor are committed to the repository in a timely manner.

##Vendor Development Team will:
1. Follow repository folder hierarchy created by Platforms Team and not deviate from or alter it without explicit permission from Platforms Team.
2. Commit code updates to the repository at least once daily or as instructed by Platforms Team while development is in-process.
3. Check for and pull any repository updates on a regular basis or as instructed by Platforms Team. 
4. Ensure that all committed code does not include extraneous files or code not directly related to or explicitly needed for a given project/product (e.g. file/folder shortcuts/aliases, temp files/code, duplicate files/code, artefact files/code, IDE-specific files/code not directly related to project/product).
5. Regularly perform bug/regression testing before committing code.
