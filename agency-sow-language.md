#Deloitte.com Custom HTML
## Agency SOW Language

###Summary

The following language should be included as part of any statement of work submitted by a vendor for custom HTML development on Deloitte.com

###Custom Interactive - Development and deployment processes

Unless otherwise specified and agreed to in writing, [agency] will follow all requirements, processes, and deliverables set forth in the following documents: “Deloitte.com Custom HTML - Vendor Requirements for Design/Development” and “Vendor Code Repository Requirements and Expectations”. *(These documents can be directly downloaded from the following link: https://deloi.tt/2AwYLRe.)*

###Custom Interactive - Cookies

As part of the European Union General Data Protection Regulation (GDPR) requirements, if any part of the custom interactive uses, sets, or otherwise leverages cookies, whether indirectly via a third party service or directly in the code itself, [agency] will inform Deloitte technical POC and work with POC to ensure interactive conforms to Deloitte’s OneTrust requirements for GDPR.

###Custom Interactive - Code warranty

[agency] warrants that the custom interactive will conform in all material aspects to the functional and other descriptions outlined in the “Deloitte.com Custom HTML - Vendor Requirements for Design/Development” document and be defect free for a period of no less than ninety (90) days after the date of launch, unless otherwise specified and agreed to in writing. [agency] agrees to fix, at its own expense, any demonstrable defects, errors, and incomplete functionality, provided that the Deloitte team gives [agency] notice of such defects, errors, and incomplete functionality during the warranty period, and that such defects, errors, and incomplete functionality are not the result of any modifications or misuse of the custom interactive or its functionality by Deloitte or its agents. This shall be Deloitte's sole and exclusive remedy and [agency’s] sole obligation with respect to any defects, errors, and incomplete functionality.