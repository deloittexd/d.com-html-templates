#Deloitte.com Custom HTML
##Vendor Requirements for Design/Development

###Summary

Vendors should follow these steps to ensure design/development approval and installation of custom HTML within Deloitte.com’s CMS (Adobe Experience Manager):

* Follow Deloitte [brand and editorial style guidelines](https://brandspace.deloitte.com) and obtain approval before coding.
* Follow all technical/usability requirements as outlined in the [HTML Fragment Playbook](https://brandspace.deloitte.com/content/index/guid/html_fragments?parent=44).
* Provide mockups showing experience across desktop, tablet, and mobile views.
* Utilize the provided [D.com HTML Page Templates repository](https://bitbucket.org/deloittexd/d.com-html-templates) for development and testing.
* Ensure that any identified analytics interaction, such as section load, button/link click, or navigation button, has a placeholder event trigger for analytics integration.
* Provide self-hosted testing URL of custom HTML using D.com Page Templates for approval.
* When approved, create a pull request per Final Code Delivery instructions.
* Where they differ, guidelines and guidance provided directly by Platforms Team should supercede all other playbook, brand, editorial, or style guidelines and requirements.

###Technical/Usability Requirements

The [HTML Fragment Playbook](https://brandspace.deloitte.com/content/index/guid/html_fragments?parent=44) explains in detail the technical/usability requirements that will ensure compatibility and compliance with Deloitte.com pages and resources. Developers should document any requirements that cannot be met and discuss them with Platforms Team.

###Browser Support Requirements

Vendors should verify that any custom HTML code renders as-expected using the [Browser Testing Matrix](https://bitbucket.org/deloittexd/d.com-html-templates/src/master/QA-Browser-Testing-Matrix.xlsx).
 
_This matrix is updated quarterly and supersedes any browser support requirements listed in the HTML Fragment Playbook._

###Design, Brand and Editorial
* Custom HTML development should follow brand guidelines as outlined in [BrandSpace](https://brandspace.deloitte.com/).
* Color palette, typography, infographics, icons, etc., should follow Deloitte.com’s [web style guide](https://brandspace.deloitte.com/content/index/guid/web_style_guide?parent=211).
* Functionality -- including modal windows, buttons, menus and other controls -- should complement existing Deloitte.com functionality.
* All copy should follow [Deloitte US Style Guide](https://brandspace.deloitte.com/content/index/guid/guidelines?parent=1).
* Vendor should provide mockups detailing experience across desktop, tablet, and mobile views. 
* All mock-ups require approval from Deloitte US National Branding and Platforms Team before vendor commences coding to ensure branding/editorial compliance.

###AEM Page Templates

Deloitte will provide [read-access to a repository](https://bitbucket.org/deloittexd/d.com-html-templates) containing HTML template files for development and testing for the following AEM template types:

1. Article page \(right rail enabled\)
2. Article page \(right rail disabled\)
3. Branded Landing page
4. Topic page

Each template type includes two versions: One that contains a simple example HTML fragment with current portal size messaging to use as a visual reference, and a second template without the fragment. These templates will include a variety of existing D.com AEM components/content as page filler. The purpose is to recreate an existing D.com page experience to develop/test custom HTML code against. Custom HTML code for Article pages will need to accommodate both 1 and 2 above.

**The purpose of these templates is to give vendors a sandbox to develop and test code against and to also provider a means of reviewing custom HTML development before vendor code is installed on AEM, with the expectation that vendors will implement their custom code in the appropriate template and provide a self-hosted link for Platforms Team and others to review before providing any files for AEM installation.**

**Vendors should use the second, non-example fragment template to code against.**

Developers may **reference** existing Deloitte.com styles and other items included in the templates, but **should not** modify or remove any components, code, or file references in the template files. In the case of naming conflicts, please ensure that alternate tags, IDs, classes, etc., are used. This is to ensure the custom HTML will work properly when integrated into the AEM platform.

For each page template, custom HTML code, and references to external assets \(CSS/JS files\) needed by the custom HTML, should be inserted between the following lines:

* **Article page \(right rail enabled\)**
    * File: /!_article-page/article-page.html
    * Lines: 517-519
* **Article page \(right rail disabled\)**
    * File: /!_article-page/article-page-norightrail.html
    * Lines: 517-519
* **Branded Landing page**
    * File: /!_branded-landing-template/branded-landing-template.html
    * Lines: 474-476
* **Topic Page**
    * File: /!_topic-page/topic-page.html
    * Lines: 718-720

###Responsive Images

On the AEM platform, responsive images are available if they follow 4:1 and 1:1 aspect ratios, provided you wish the final sizes to be similar to the ones lists below.

To achieve this, use the following code replacing the folder and filenames:

* data-orgsrc – add the image path, the images should be in these folders to work \(header_images, promo_images\).
* data-aspectratio – use 4:1 or 1:1 depending on the image being used.
* Example:
    ```
    <img class="responsive-img" src="" data-orgsrc="/content/dam/Deloitte/us/Images/header_images/us-laborwise-banner.jpg" alt="" data-aspectratio="4:1"/>
    ```
 
Header image   4:1 aspect ratio
Desktop: 1400x350
Tablet: 1024x256
Mobile: 768x192

Promo image 1:1 aspect ratio
Desktop: 231x231
Tablet: 350x350
Mobile: 250x250

For the sake of off-platform testing you may want to consider the Picturefill JS library (http://scottjehl.github.io/picturefill/) for testing of responsive images in the page layout.

###Analytics

Platforms Team will provide vendor with guidance on what specific interactions should have event listeners attached to them for analytics integration. We find it’s easiest if a vendor creates a separate “analytics.js” file for this purpose, referenced by the main HTML file. That analytics file should contain only the event listeners identified as needed by the Platforms Team, and can simply be printed out to the console. As part of AEM integration, Platforms team will replace the console.log statements with analytics-specific tags and verify everything is working as expected.

###AEM Property Variables

AEM allows for CMS property variables to be used inline with custom HTML code, using the following expression: 
    ```
    ${my-variable-name}
    ```

These variables can be directly exposed in the HTML code, but must be passed as a javascript variable to be accessible in javascript not directly coded inline in the custom HTML.

Example: 
    ```
    var myVariable = ${my-variable-name};
    ```

Platforms Team will identify any custom AEM property variables to be used and work with vendor development team to ensure they’re properly integrated into the custom HTML code.

Vendor code should account for AEM properties not being present and gracefully removing any AEM generated artefacts as a result. Code should check that a property has a value other than the AEM generated property name, and handle it accordingly. We understand this can be difficult without a vendor being able to directly develop in our AEM environment, which is part of what our integration testing is intended to surface and something Platforms Team can work with a vendor on to best accommodate and validate. 

###Custom HTML - Mockup review

All projects require review and approval of static mock-ups by Deloitte US National Branding and Platforms Team before vendor commences coding to ensure branding/editorial compliance.

###Custom HTML - Page Template Review

When custom HTML is completed, vendor will provide Platforms Team a self-hosted URL showing the custom development **within** the appropriate AEM page templates for approval. The purpose of this is to provide a standalone, self-contained version of the custom development to assess, approve, and/or otherwise update if needed, before delivery of final code.

Unless otherwise not possible, vendor code should be “future friendly” by accounting for, among other things, one-to-many items being possible in a given component, or text/images changing after a component has launched. For example: where a mockup might show a fixed number of  items for a given component, vendor should assume there might be more or fewer items and advise on best UX/design in keeping a component flexible and content agnostic.

###Custom HTML - Functional Requirements Documentation

Vendor will provide Platforms Team with a functional requirements document outlining the requirements for the content management of the custom HTML, including areas of customization, style and behavior, and associated AEM properties. Vendor and Platforms team will collaborate, revise, and update the document as-needed following mockup review through product deployment.

####Document example
> * **Deloitte Services LP**
> * **Slider Fragment**
> * **Requirements for content management**
> 
> **Areas of customization by content manager:**
> 
> * Container ID
> * Slides \(Title, text, image, alt text, and CTA text/url/new window\)
> * Autoplay Enabled
> * Black or White background
> * If the position of the image and text should be swapped on desktop \(defaults to image on left, content on right\)
>  
> **Style and behavior:**
> 
> * Fragment will render at the top of the page directly underneath the Deloitte header and navigation.
> * The image will sit on the left of the slide and cover 2/3rds of the width, while the content \(consisting of the Title, Text, and CTAs\) will sit on the right and cover the final third. The position of the image and content can be swapped.
> * The fragment will automatically render navigation dots and arrows, which will sit at the bottom of the slide content area.
> * If autoplay is enabled, the slide will change every 8 seconds, and will pause if the mouse is hovered over the slider.
> * On mobile, the image and content will be full width and the image will stack on top of the content.
>  
> |Customization area|AEM Property|Value|
> |------------------|------------|-----|
> |Container ID|slider-fragment-id|Text \(lowercase and dashed\)|
> |Slider Autoplay|slider-autoplay|“true” or “false” \(defaults to false\)|
> |Number of Slides|slider-number-of-items|Integer \(1-10\)|
> |Background color \(black or white\)|slider-bg-white-or-black|“black” or “white” \(defaults to “white”\)|
> |Swap slider image and content position|slider-image-on-right|“true” or “false” \(defaults to “false”\)|
> |Slide 1 Title|slider-item1-title|Text|
> |Slide 1 Content|slider-item1-text|Text|
> |Slide 1 Image|slider-item1-image|URL|
> |Slide 1 Image Alt Text|slider-item1-alt-text|Text|
> |Slide 1 CTA URL|slider-item1-cta-url|URL \(pipe separated for multiple values\)|
> |Slide 1 CTA Text|slider-item1-cta-text|Text \(pipe separated for multiple values\)|
> |Slide 1 CTA Open link in new window|slider-item1-cta-new-window|“yes” or “no” \(pipe separated for multiple values\)|

###Custom HTML - Final Code Delivery

Final custom HTML code delivery of should be made through a [Pull Request](https://www.atlassian.com/git/tutorials/making-a-pull-request) to the Final Code repository identified by Platforms Team \(separate from the above Page Templates repository\).

Files in the final code repository **should only** include static assets and code directly related to the custom HTML. No extraneous code from the Page Template files should be included.

Folder hierarchy for the final code repository **must** conform the following file structure. Any references to static assets or other files **must** similarly reference this file structure.

**Static Assets**

* Javascript 
    * /content/dam/html/us/[fragment-name]l/test-release/js/
* CSS
    * /content/dam/html/us/[fragment-name]/test-release/css/
* Images
    * /content/dam/html/us/[fragment-name]/test-release/images/
* HTML
    * /etc/advanced-html/us/[fragment-name]/test-release/

All custom HTML files should also be in lowercase and use hyphens to separate distinct words.
**Ex:** tabbed-carousel.css, tabbed-carousel.html -not- TabbedCarousel.css.

During AEM Installation, Platforms Team will move repository code from **/test-release/** into **/current-release/** once code is approved and ready for final deployment. Platforms team will also update code references to point to /current-release/.

###AEM Installation

AEM access is not available to external agencies and developers. Files must be installed by the Deloitte.com team for any interim testing and final integration. After Platforms Team approves and merges the Pull Request, they will schedule custom HTML installation \(or updates to an existing installation\) with appropriate parties and inform stakeholders when updates have been deployed for review in Deloitte.com’s AEM Preview environment.
