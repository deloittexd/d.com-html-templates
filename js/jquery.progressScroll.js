var cookieHeight = $(".cookie-container").outerHeight(true);
var headerHeight = $(".header-container").outerHeight(true);
var breadcrumbHeight = $(".breadcrumb-section-bar").outerHeight(true);

$.fn.progressScroll = function(options){
	var settings = $.extend({
		fontSize : 20,
		fontFamily : 'sans-serif',
		color : '#009ACF',
		height : '5px',
		textArea : 'lite',
	}, options);

	// namespace
	var progress = {};
	if(settings.textArea === 'dark'){
		$('.scrollWrapper').css({"background-color": "rgba(0,0,0,0.75)"});
		$('.scrollWrapper h4').css({"color": "white"});
	} else {
		$('.scrollWrapper').css({"background-color": "rgba(255,255,255,1)"});
		$('.scrollWrapper h4').css({"color": "black"});

	}
	progress.targetScroll = 0;// scroll amount
	progress.headHeight = $('.header-wrapper').outerHeight()// fixed nav height
	progress.screenh = $(window).height() //+ $('#footer-section').outerHeight(); //window height
	progress.divHeight = $(this).outerHeight() + $('#footer-section').outerHeight();// content height
	progress.numberOfH2 = $('h2').length;// lenght of que points for progress
    
    
    $('.content-page-info .left-content-section h3').each(function(id) {
        
        $(this).attr('data-id-link',$(this).text().replace(' ','-')+'-'+id);
        
        var per = Math.round((($(this).position().top-(progress.headHeight+22))/((progress.divHeight ) - progress.screenh))*100);
        
     //   console.log($(this).text(), per);
        
        var title = $(this).text();// logic for fallback content
     //   var sub = $(this).closest('.box').find('.sub-headline').text() ;// logic for fallback content
        if(per>=0 && per < 100){
            $('.scrollWrapper').append(
                '<a class="link-dot" style="left:'+(per)+'%;" '+
                    ' data-link="'+$(this).text().replace(" ","-")+ '-'+id+'" '+
                    'data-toggle="popover"'+
                '</a>'
            );

            $('.link-dot').popover({ 
                html : 'true',
                placement: 'bottom',
                content: '<i class="icon-bookmark-o"></i><span>' + title + '</span>'
            }); 
        }	
    });

    setTimeout(function(){
               $('[data-toggle="popover"]').each(function(){
                  $(this).on('mouseover',function(){
                     $(this).popover('show');
					
                 });

                  $(this).on('mouseout',function(){
					 
                     $(this).popover('hide');
                 })
              })
    },1000);

    // que point scrollTo funcitons
    $('.scrollWrapper .link-dot').on('click touch',function(){
        var $ref = $(this).data('link') //+ '-' + $(this).index();
    //    console.log('[data-id-link="'+$ref+'"]',$ref)
        $('html, body').animate({
            scrollTop: $('[data-id-link="'+$ref+'"]').offset().top - (cookieHeight + headerHeight + breadcrumbHeight + 60)
        }, 400);
    })
    $(window).scroll(function() {
	  	var scrollAmount = $(this).scrollTop() - progress.headHeight ;
	  	var scrollPercent = ((scrollAmount)/(progress.divHeight - progress.screenh))*100;
          if(scrollPercent>100)scrollPercent=100;
		// console.log("scroll amount "+scrollAmount);
		// console.log("scroll percent "+scrollPercent+"%");
		// console.log("screen height"+progress.screenh)

		//blank out the text if above the first h2 tag
		// if(scrollAmount <= $('h2:first').position().top){
		// 	// $('.scrollWrapper h3').text('');
		// }
        
        // add percentage to label based on scroll
        $('.scrollWrapper .progress-indicator .label').text(Math.round(scrollPercent)+ '%').parent().css('left',scrollPercent+'%')
        
        //show or hide label depending on scroll position
        if(scrollPercent >= 1 && scrollPercent <= 99){
            $('.progress-indicator').show();        
        }else{
            $('.progress-indicator').hide();
        }

		//everytime it passes an h2 it grabs it's text
		$('.content-page-info .left-content-section h3').each(function() {
			if(scrollAmount + progress.headHeight + 30 >= $(this).position().top){
				var head = $(this).text();
            //    var sub = $(this).closest('.box').find('.sub-headline').text();
                var time = $('.scrollWrapper .eta-mock').text();
                var text = '<span class="label">' + head + "</span>"; /* + 
                 '<span class="headline"> | '+ sub + '</span>'; */
           //      '<span class="eta"><span class="icon-clock"></span>' + time + ' read</span>';
	    		$('.scrollWrapper h4').html(text);
	    		// console.log("this pos top "+$(this).position().top)
	    		// $('.scroll-bar').toggleClass('orange');
			};
		});

		//calculate scroll amount
	    $('.scroll-bar').css('width', scrollPercent+'%' );
	    // $('.scroll-bar').css('opacity', scrollPercent/100 );
	    if( scrollAmount >= progress.targetScroll){
	    	$('.scrollWrapper').removeClass('hidden');
		} else {
			$('.scrollWrapper').addClass('hidden');
		};
	 
	}); //end window scroll

	var $el = $('.scroll-bar').css(settings); 
	return $el;
// });
}