var headersiteSelectorList;
var selectSiteSelector;
var adjustSiteSelectorLocation;
var sc_language = "en";
var sc_country = "global";
var siteDomain = "www2.deloitte.com";
var isTMP = "false";$( document ).ready(function() {
	var countryJsonData={};
	var locale = sc_language+'_'+sc_country.toUpperCase();

	if(typeof wcmMode === 'undefined'){
		wcmMode = "DISABLED";
	}

	var processJson = function(data){
		if("undefined" !== typeof data.countries && data.countries.length > 0){
			var nowTime = new Date();
			
			countryJsonData = data;
			countryJsonData.aemMode = wcmMode;
			countryJsonData.createdAt = nowTime.getTime();
			
			if(Modernizr.localstorage){
				localStorage.setItem(locale, JSON.stringify(countryJsonData));
			}

			renderControl();
		}else{
			console.debug("Invalid JSON for SS");
		}
	};
	
    var callJson = function() {
        var cacheBustDate = new Date();
        var jsUrl = siteDomain + '/' + locale + '.js?x=' + cacheBustDate.getTime();

        if (isTMP && $.browser.msie && $.browser.versionNumber <= 9 && window.XDomainRequest) {
            //Cross Domain request for JavaScript on MSIE 9 and lower - use XDomainRequest
            var xdr = new XDomainRequest();
            xdr.open("get", jsUrl);
            xdr.onload = function() {
                var responseJson = JSON.parse(xdr.responseText);
                processJson(responseJson);
            };
            xdr.send();
        } else {
            $.ajax({
                url: jsUrl,
                dataType: "json",
                success: function(responseJson) {
                    processJson(responseJson);
                }
            });
        }
    }

    var renderControl = function() {
        $.each(countryJsonData.countries, function(i, j) {
            var countryLocale = ((j.locale).split("/"))[0] + "_" + ((j.locale).split("/"))[1].substring(0, 2);
            var siteUl = $('#site-selector ul.channel-site,#site-selector-global ul.channel-site');
            if (j.redirectLink != undefined) {
                if (countryJsonData.aemMode == "EDIT") {
                    link = "/" + j.locale;
                } else {
                    link = j.redirectLink;
                }
            } else {
                link = "/" + j.locale;
            }
            var link;
            if ((link.trim().substring(0, 4)).localeCompare('http') != 0) {
                link = siteDomain + link;
            }
            if (!(j.activateStatus == "false" && countryJsonData.aemMode == "DISABLED")) {
                $(siteUl).append("<li ><a href=" + link + " class='site_" + countryLocale + "'> " + "<span class='no-click-close-local'>" + j.localeLanguageTranslation + "</span><span class='no-click-close-trans'>" + j.memberFirmLanguageTranslation + "</span><span class='no-click-close-abbr' style='display:none'  >" + j.abbr + "</span></a></li>");
            }

            $(".site_" + countryLocale).click(function() {
                var c_name = "CountryLocale";
                var exDays = 90;
                var date = new Date();
                date.setTime(date.getTime() + (exDays * 24 * 60 * 60 * 1000));
                var c_value = countryLocale;
                if (document.cookie = c_name + "=" + c_value + "; expires=" + date.toGMTString() + "; path=/") {
                    checkCookie(c_name, c_value, date);
                    return true;
                } else {
                    return false;
                }

                 });
                 function getCookie(cname) { 
                    var name = cname + "=";
                    var ca = document.cookie.split(';');
                    for(var i=0; i<ca.length; i++) {
                        var c = ca[i];

                        while (c.charAt(0)==' ') c = c.substring(1);
                        if (c.indexOf(name) == 0) {
                            return c.substring(name.length, c.length);
                        }
                    }
                    return "";
                 }

                 function checkCookie(c_name, c_value, date) {
                    var countryLocaleCookie=getCookie("CountryLocale");
                       if (countryLocaleCookie != "" && countryLocaleCookie != null) {
                           document.cookie = c_name + "="+c_value+"; expires=" + date.toGMTString() + "; path=/"
                    }
                }                 
		 });
			 
		 /*Accessibilty code*/
        $('#site-selector ul.channel-site li a,#site-selector-global ul.channel-site li a').focus(function() {
            $(this).parent().css('background-color', '#f9f9f9');
        });
        $('#site-selector ul.channel-site li a#site-selector-global ul.channel-site li a').focusout(function() {
            $(this).parent().css('background-color', 'none');
        });
        $("#site-selector ul.channel-site li a:last").on("focusout", function() {
			 //$(this).next().focus();
			$('.siteselector').removeClass("site-autodrop");
			 $('.modal-backdrop').addClass("visibility-hidden");
			 $('#location').removeClass("site-autodrop");
			 $('.location-link.no-click-close').removeClass("site-autodrop");
			 $(".location-container").slideUp().removeClass("open");
			 $('a.location-link').removeClass("active");

        });
        $("#site-selector-global ul.channel-site li a:last").on("focusout", function() {
            $('.siteselector').removeClass("site-autodrop");
            $('.modal-backdrop-global').addClass("visibility-hidden");
            $("#site-selector-global").slideUp().removeClass("open");
            $('.global-site-selector > div.site-selector .site-info-global').addClass("visibility-hidden");

        });
        if (countryJsonData.aemMode == "DISABLED" || (window.location.href.indexOf('careers-deloitte-com') != -1)) {
            $.each(countryJsonData.count, function(m, n) {
                $('div.site-info,div.site-info-global').append('<span >' + n.sitesPublish + '</span>');
            });
        } else {
            $.each(countryJsonData.count, function(m, n) {
                $('div.site-info,div.site-info-global').append('<span >' + n.sitesAuthor + '</span>');
            });
        }

        var options = {
            valueNames: ['no-click-close-local', 'no-click-close-trans', 'no-click-close-abbr']
        };
        headersiteSelectorList = new List('site-options', options);
        siteSelectorList = new List('site-options-global', options);
        var modalContainer = $(document.createElement('div')).addClass('modal-backdrop').addClass('fade').addClass('in').addClass('visibility-hidden');

		//$("ul.topline-nav").append(modalContainer);
		$("nav.navigation").append(modalContainer);
		if ((matchMedia('(max-width: 767px)').matches) && (matchMedia('(min-width: 260px)').matches)) {

			$("nav.navigation div.modal-backdrop").remove();
		}

        var modalContainerGlobal = $(document.createElement('div')).addClass('modal-backdrop-global').addClass('fade').addClass('in').addClass('visibility-hidden');
        $("div.global-site-selector > div.site-selector").append(modalContainerGlobal);

        selectSiteSelector = function(e) {
            e.preventDefault();
            $("li.subnav-item").css("border-bottom", "none");
            $('#site-options').removeClass("visibility-hidden");
            if ($('.location-container').hasClass("open")) {
                $('.location-container').slideUp(200).removeClass("open");
                headersiteSelectorList.search();
                $(".location-container input.gsc-input").val("");
                $('#location').removeClass("site-autodrop");
                $('.location-link.no-click-close').removeClass("site-autodrop");
                $('.siteselector').removeClass("site-autodrop");
                $('.modal-backdrop').addClass("visibility-hidden");
                $('a.location-link').removeClass("active");
                $(".location-link").attr('aria-expanded', false);

            } else {
                headersiteSelectorList.search();
                setTimeout(function() {
                    $(".location-container input.gsc-input").focus();
                }, 300);
                var isIE11 = !!navigator.userAgent.match(/Trident.*rv[ :]*11\./);
                if (isIE11) {
                    $('#site-selector ul.channel-site li a').hover(function() {
                        $(".location-container input.gsc-input").blur();
                    });
                    $(".location-container input.gsc-input").hover(function() {
                        $(".location-container input.gsc-input").focus();
                    });
                }
                var ie = (!!window.ActiveXObject && +(/msie\s(\d+)/i.exec(navigator.userAgent)[1])) || NaN;
                if (ie === 9) {
                    $('.location-container input.gsc-input').keypress(function(e) {
                        $(".location-container .search-option").css("visibility", "hidden");
                    });
                }
				$('#location').addClass("site-autodrop");
				$('.location-link.no-click-close').addClass("site-autodrop");
				$('.modal-backdrop').removeClass("visibility-hidden");
				var addDelay = 0;
				 //$(".location-container form input.gsc-input").focus();
				if ($("li.subnav-item div.sub-nav.open").length > 0) {
					$("li.subnav-item div.sub-nav.open").slideUp().removeClass("open");
					$("li.subnav-item a").removeClass("active");
					$(".location-link").attr('aria-expanded',false);
					//$(".location-container form input.gsc-input").focus();
					addDelay = 650;

                }

                $(".location-link").attr('aria-expanded', true);
                $('a.location-link').addClass("active");
                $('.location-container').delay(addDelay).slideDown().addClass("open");
            }
        }

        $(".modal-backdrop ").click(function(e) {
            $(".location-container input.gsc-input").val("");
            setTimeout(function() {
                $(".location-container input.gsc-input").focus();
            }, 300);
            headersiteSelectorList.search();

            if (!($(e.target).hasClass("no-click-close") || $(e.target).hasClass("ssb_sb"))) {
                if ($("li.subnav-item div.sub-nav.open").length > 0) {
                    $("li.subnav-item div.sub-nav.open").slideUp().removeClass("open");
                    //$("li.subnav-item div.sub-nav.open").animate({ height: 0 }, 400, function () { $("li.subnav-item div.sub-nav.open").css({ height: "auto" }); $("li.subnav-item div.sub-nav.open").hide().removeClass("open") });
                    $("li.subnav-item a.active").removeClass("active");
                }
                if ($(".location-container").hasClass("open")) {
                    $(".location-container").slideUp(200).removeClass("open");
                    $('.siteselector').removeClass("site-autodrop");
                    $('.modal-backdrop').addClass("visibility-hidden");
                    $('#location').removeClass("site-autodrop");
                    $('.location-link.no-click-close').removeClass("site-autodrop");
                    $('a.location-link').removeClass("active");
                }
            }
        });
        $(".modal-backdrop-global ").click(function(e) {
            $(".global-site-selector > div.site-selector input.gsc-input").val("");

            siteSelectorList.search();

            if (!($(e.target).hasClass("no-click-close") || $(e.target).hasClass("ssb_sb"))) {

                if ($(".global-site-selector > div.site-selector #site-selector-global").hasClass("open")) {
                    $(".global-site-selector > div.site-selector #site-selector-global").slideUp(200).removeClass("open");
                    $('.global-site-selector > div.site-selector .site-info-global').addClass("visibility-hidden");
                    $('.modal-backdrop-global').addClass("visibility-hidden");
                    $('div.global-site-selector > div.site-selector').find('#site-options-global').css('z-index', '');
                }
            }
        });

        $(document).bind("keyup", null, function(e) {
            if (e.which == 27) {
                if ($(".location-container").hasClass("open")) {
                    $('.siteselector').removeClass("site-autodrop");
                    $('.modal-backdrop').addClass("visibility-hidden");
                    $('#location').removeClass("site-autodrop");
                    $('.location-link.no-click-close').removeClass("site-autodrop");
                    $(".location-container").slideUp().removeClass("open");
                    $('a.location-link').removeClass("active");
                    $(".location-container input.gsc-input").val("");
                    setTimeout(function() {
                        $(".location-container input.gsc-input").focus();
                    }, 100);
                }
                if ($(".global-site-selector > div.site-selector #site-selector-global").hasClass("open")) {
                    $('.modal-backdrop-global').addClass("visibility-hidden");
                    $('.global-site-selector > div.site-selector .site-info-global').addClass("visibility-hidden");
                    $(".global-site-selector > div.site-selector #site-selector-global").slideUp().removeClass("open");
                    $(".global-site-selector > div.site-selector input.gsc-input").val("");
                    $('div.global-site-selector > div.site-selector').find('#site-options-global').css('z-index', '');
                    setTimeout(function() {
                        $(".global-site-selector > div.site-selector input.gsc-input").blur();
                    }, 100);
                }
            }
        });
        $('.gsc-input').keydown(function(e) {
            if (e.keyCode == 40) {
                $("a[class^='site_']:first").focus().closest('li');
            }
        });

        $("a[class^='site_']").each(function() {
            $(this).keydown(function(e) {
                if (e.keyCode == 40) {
                    $(this).closest('li').next().find("a[class^='site_']").focus();
                }
                if (e.keyCode == 38) {
                    $(this).closest('li').prev().find("a[class^='site_']").focus();
                }
            })
        })



    }

    //Attempt to fetch JSON from local cache
    if (Modernizr.localstorage && localStorage.getItem(locale) !== null && localStorage.getItem(locale) !== "undefined") {
        var currentTimestamp = Math.round(new Date().getTime()) / 10000;
        var storedTimestamp = 0;
		
		var localJSON = {};

		var tempLocalJSON = JSON.parse(localStorage.getItem(locale));
		//validate the local JSON
		if(typeof tempLocalJSON.createdAt !== "undefined"  && tempLocalJSON.createdAt > 0 && typeof tempLocalJSON.countries !== "undefined" && tempLocalJSON.countries.length > 0){
			localJSON = tempLocalJSON;
			storedTimestamp = localJSON.createdAt / 10000;
		}
		
        if((currentTimestamp - storedTimestamp) <= 86400) {
            /* Less than 24 hours */
			countryJsonData = localJSON;
        }
    }


    //If we do not have a valid JSON (from cache) make a JSON request
    if ("undefined" === typeof countryJsonData.countries || countryJsonData.countries.length <= 0) {
        callJson();
    } else {
        renderControl();
    }

    adjustSiteSelectorLocation = function() {
        if (matchMedia('(min-width: 768px)').matches && ($('.countryselector').length > 0)) {
            var siteSelDropText = $(".location-link").position();
            var locBoxWidth = $('.location-container').outerWidth(true); // 300
            $(".location-container").css('right', $(window).width() - siteSelDropText.left - $('.location-link').outerWidth(true));
        }
    }


});

$(window).load(function() {
    $('.location-link').click(function(e) {
        selectSiteSelector(e);
    });
    $('.global-site-selector > div.site-selector input').on('focus', function(e) {
        e.preventDefault();
        if ((matchMedia('(max-width: 767px)').matches) && (matchMedia('(min-width: 260px)').matches)) {
            $("#header").css("position", "fixed");
            $('html, body').stop().animate({ scrollTop: 50 }, 500, 'swing');
        }
        $('div.global-site-selector > div.site-selector').find('#site-options-global').css('z-index', '1999');
        $('.global-site-selector > div.site-selector #site-selector-global').removeClass("visibility-hidden").css('overflow-y', 'scroll');
        $('.global-site-selector > div.site-selector .site-info-global').removeClass("visibility-hidden");
        $('.modal-backdrop-global').removeClass("visibility-hidden");
        siteSelectorList.search();
        var IE = window.navigator.userAgent.indexOf("MSIE ");
        if (IE) {
            //console.log("IE10");
            $('#site-options-global ul.channel-site li a').hover(function() {

                $(this).focus();
            });
            $(".site-options-global input.gsc-input").hover(function() {
                $(".site-options-global input.gsc-input").focus();
            });
        }
        var ie = (!!window.ActiveXObject && +(/msie\s(\d+)/i.exec(navigator.userAgent)[1])) || NaN;
        if (ie === 9) {
            $('.global-site-selector > div.site-selector input.gsc-input').keypress(function() {
                $(".global-site-selector > div.site-selector #site-selector-global .search-option").css("visibility", "hidden");
            });
        }

        $('.modal-backdrop-global').removeClass("visibility-hidden");
        var addDelay = 0;
        $('.global-site-selector > div.site-selector #site-selector-global').show().addClass("open");
    });

    if (matchMedia('(max-width: 320px)').matches) {

        var cookieHeight = $('.cookie-container').outerHeight(true);
        var locationContainerHeight = cookieHeight + 53 + '%';

        var locationContainerSiteinfoTop = cookieHeight + 230 + 'px';
        var headerSiteSelectorHeight = cookieHeight + 26.3 + 'em';


        $('.location-container #site-selector').css("height", locationContainerHeight);
        $('.location-container .site-info').css("top", locationContainerSiteinfoTop);

    }
    $(".global-site-selector .global-site-selector-close a").click(function() {
        $(".global-site-selector").animate({ 'height': 'toggle' }, 700, "linear");
        // to put focus on featured option
        $(".featured-trending-container .toggle-links .gotoslide1 .featured").focus();
    });
});

$(window).resize(function() {
    if ($(".location-container ").hasClass("open")) {
        adjustSiteSelectorLocation();
    }
});