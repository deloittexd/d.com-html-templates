{
	"countries" : [{
			"localeLanguageTranslation" : "Global (English)",
			"memberFirmLanguageTranslation" : "Global (English)",
			"locale" : "global/en.html?icid=site_selector_global",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Albania & Kosova (English)",
			"memberFirmLanguageTranslation" : "Albania (English)",
			"locale" : "al/en.html?icid=site_selector_al",
			"redirectLink" : "http://www2.deloitte.com/al/sq.html?icid=site_selector_al",
			"activateStatus" : "true",
			"abbr" : "AL"
		}, {
			"localeLanguageTranslation" : "Shqiperia (Shqiptar)",
			"memberFirmLanguageTranslation" : "Albania (Albanian)",
			"locale" : "al/sq.html?icid=site_selector_al",
			"redirectLink" : "http://www2.deloitte.com/al/sq.html?icid=site_selector_al",
			"activateStatus" : "true",
			"abbr" : "AL"
		}, {
			"localeLanguageTranslation" : "Algérie (Français)",
			"memberFirmLanguageTranslation" : "Algeria (French)",
			"locale" : "dz/fr.html?icid=site_selector_dz",
			"activateStatus" : "true",
			"abbr" : "AL"
		}, {
			"localeLanguageTranslation" : "Angola (Português)",
			"memberFirmLanguageTranslation" : "Angola (Portuguese)",
			"locale" : "ao/pt.html?icid=site_selector_ao",
			"activateStatus" : "true",
			"abbr" : "AN"
		}, {
			"localeLanguageTranslation" : "Argentina (Español)",
			"memberFirmLanguageTranslation" : "Argentina (Spanish)",
			"locale" : "ar/es.html?icid=site_selector_ar",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Armenia (English)",
			"memberFirmLanguageTranslation" : "Armenia (English)",
			"locale" : "am/en.html?icid=site_selector_am",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Հայաստան (Հայերեն)",
			"memberFirmLanguageTranslation" : "Armenia (Armenian)",
			"locale" : "am/am.html?icid=site_selector_am",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Australia (English)",
			"memberFirmLanguageTranslation" : "Australia (English)",
			"locale" : "au/en.html?icid=site_selector_au",
			"redirectLink" : "/au/en.html?icid=site_selector_au",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Österreich (Deutsch)",
			"memberFirmLanguageTranslation" : "Austria (German)",
			"locale" : "at/de.html?icid=site_selector_at",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Azərbaycan (Azeri)",
			"memberFirmLanguageTranslation" : "Azerbaijan (Azeri)",
			"locale" : "az/az.html?icid=site_selector_az",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Azerbaijan (English)",
			"memberFirmLanguageTranslation" : "Azerbaijan (English)",
			"locale" : "az/en.html?icid=site_selector_az",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Bahamas (English)",
			"memberFirmLanguageTranslation" : "Bahamas (English)",
			"locale" : "bs/en.html?icid=site_selector_bs",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Bahrain (English)",
			"memberFirmLanguageTranslation" : "Bahrain (English)",
			"locale" : "bh/en.html?icid=site_selector_bh",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Barbados (English)",
			"memberFirmLanguageTranslation" : "Barbados (English)",
			"locale" : "bb/en.html?icid=site_selector_bb",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Беларусь (Русский)",
			"memberFirmLanguageTranslation" : "Belarus (Russian)",
			"locale" : "by/ru.html?icid=site_selector_by",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Belarus (English)",
			"memberFirmLanguageTranslation" : "Belarus (English)",
			"locale" : "by/en.html?icid=site_selector_by",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Belgium (English)",
			"memberFirmLanguageTranslation" : "Belgium (English)",
			"locale" : "be/en.html?icid=site_selector_be",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Bénin (Français)",
			"memberFirmLanguageTranslation" : "Benin (French)",
			"locale" : "bj/fr.html?icid=site_selector_bj",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Bermuda (English)",
			"memberFirmLanguageTranslation" : "Bermuda (English)",
			"locale" : "bm/en.html?icid=site_selector_bm",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Bolivia (Español)",
			"memberFirmLanguageTranslation" : "Bolivia, Plurinational State of (Spanish)",
			"locale" : "bo/es.html?icid=site_selector_bo",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Bosna i Hercegovina (Bosanski)",
			"memberFirmLanguageTranslation" : "Bosnia and Herzegovina (Bosnian)",
			"locale" : "ba/bs.html?icid=site_selector_ba",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Bosnia and Herzegovina (English)",
			"memberFirmLanguageTranslation" : "Bosnia and Herzegovina (English)",
			"locale" : "ba/en.html?icid=site_selector_ba",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Brasil (Português)",
			"memberFirmLanguageTranslation" : "Brazil (Portuguese)",
			"locale" : "br/pt.html?icid=site_selector_br",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Brazil (English)",
			"memberFirmLanguageTranslation" : "Brazil (English)",
			"locale" : "br/en.html?icid=site_selector_br",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "British Virgin Islands (English)",
			"memberFirmLanguageTranslation" : "British Virgin Islands (English)",
			"locale" : "vg/en.html?icid=site_selector_vg",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Brunei Darussalam (English)",
			"memberFirmLanguageTranslation" : "Brunei Darussalam (English)",
			"locale" : "bn/en.html?icid=site_selector_bn",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "България (Български)",
			"memberFirmLanguageTranslation" : "Bulgaria (Bulgarian)",
			"locale" : "bg/bg.html?icid=site_selector_bg",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Bulgaria (English)",
			"memberFirmLanguageTranslation" : "Bulgaria (English)",
			"locale" : "bg/en.html?icid=site_selector_bg",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Burundi (English)",
			"memberFirmLanguageTranslation" : "Burundi (English)",
			"locale" : "bi/en.html?icid=site_selector_bi",
			"activateStatus" : "false"
		}, {
			"localeLanguageTranslation" : "Cameroun (Français)",
			"memberFirmLanguageTranslation" : "Cameroon (French)",
			"locale" : "cm/fr.html?icid=site_selector_cm",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Cameroon (English)",
			"memberFirmLanguageTranslation" : "Cameroon (English)",
			"locale" : "cm/en.html?icid=site_selector_cm",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Canada (English)",
			"memberFirmLanguageTranslation" : "Canada (English)",
			"locale" : "ca/en.html?icid=site_selector_ca",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Canada (Français)",
			"memberFirmLanguageTranslation" : "Canada (French)",
			"locale" : "ca/fr.html?icid=site_selector_ca",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Cayman Islands (English)",
			"memberFirmLanguageTranslation" : "Cayman Islands (English)",
			"locale" : "ky/en.html?icid=site_selector_ky",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Channel Islands and Isle of Man (English)",
			"memberFirmLanguageTranslation" : "Channel Islands and Isle of Man (English)",
			"locale" : "xb/en.html?icid=site_selector_xb",
			"redirectLink" : "http://www2.deloitte.com/uk/en.html?icid=site_selector_xb",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Chile (Español)",
			"memberFirmLanguageTranslation" : "Chile (Spanish)",
			"locale" : "cl/es.html?icid=site_selector_cl",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "中国 (简体中文)",
			"memberFirmLanguageTranslation" : "China (Chinese)",
			"locale" : "cn/zh.html?icid=site_selector_cn",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "China (English)",
			"memberFirmLanguageTranslation" : "China (English)",
			"locale" : "cn/en.html?icid=site_selector_cn",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Colombia (Español)",
			"memberFirmLanguageTranslation" : "Colombia (Spanish)",
			"locale" : "co/es.html?icid=site_selector_co",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Congo (Français)",
			"memberFirmLanguageTranslation" : "Congo (French)",
			"locale" : "cg/fr.html?icid=site_selector_cg",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Costa Rica (Español)",
			"memberFirmLanguageTranslation" : "Costa Rica (Spanish)",
			"locale" : "cr/es.html?icid=site_selector_cr",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Cambodia (English)",
			"memberFirmLanguageTranslation" : "Country_kh (English)",
			"locale" : "kh/en.html?icid=site_selector_kh",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Country_x1 (English)",
			"memberFirmLanguageTranslation" : "Country_x1 (English)",
			"locale" : "x1/en.html?icid=site_selector_x1",
			"activateStatus" : "false"
		}, {
			"localeLanguageTranslation" : "Hrvatska (Hrvatski)",
			"memberFirmLanguageTranslation" : "Croatia (Croatian)",
			"locale" : "hr/hr.html?icid=site_selector_hr",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Croatia (English)",
			"memberFirmLanguageTranslation" : "Croatia (English)",
			"locale" : "hr/en.html?icid=site_selector_hr",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Cyprus (English)",
			"memberFirmLanguageTranslation" : "Cyprus (English)",
			"locale" : "cy/en.html?icid=site_selector_cy",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Česká republika (Čeština)",
			"memberFirmLanguageTranslation" : "Czech Republic (Czech)",
			"locale" : "cz/cs.html?icid=site_selector_cz",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Czech Republic (English)",
			"memberFirmLanguageTranslation" : "Czech Republic (English)",
			"locale" : "cz/en.html?icid=site_selector_cz",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "République démocratique du Congo (Français)",
			"memberFirmLanguageTranslation" : "Democratic Republic of the Congo (French)",
			"locale" : "cd/fr.html?icid=site_selector_cd",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Democratic Republic of the Congo (English)",
			"memberFirmLanguageTranslation" : "Democratic Republic of the Congo (English)",
			"locale" : "cd/en.html?icid=site_selector_cd",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Danmark (Dansk)",
			"memberFirmLanguageTranslation" : "Denmark (Danish)",
			"locale" : "dk/da.html?icid=site_selector_dk",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "República Dominicana (Español)",
			"memberFirmLanguageTranslation" : "Dominican Republic (Spanish)",
			"locale" : "do/es.html?icid=site_selector_do",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Dutch Caribbean (English)",
			"memberFirmLanguageTranslation" : "Dutch Caribbean (English)",
			"locale" : "an/en.html?icid=site_selector_an",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Ecuador (Español)",
			"memberFirmLanguageTranslation" : "Ecuador (Spanish)",
			"locale" : "ec/es.html?icid=site_selector_ec",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Egypt (English)",
			"memberFirmLanguageTranslation" : "Egypt (English)",
			"locale" : "eg/en.html?icid=site_selector_eg",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "El Salvador (Español)",
			"memberFirmLanguageTranslation" : "El Salvador (Spanish)",
			"locale" : "sv/es.html?icid=site_selector_sv",
			"redirectLink" : "http://www2.deloitte.com/sv/es.html?icid=site_selector_sv",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "El Salvador (English)",
			"memberFirmLanguageTranslation" : "El Salvador (English)",
			"locale" : "sv/en.html?icid=site_selector_sv",
			"redirectLink" : "http://www2.deloitte.com/sv/es.html?icid=site_selector_sv",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Guinée equatoriale / Guinée équatoriale (Français)",
			"memberFirmLanguageTranslation" : "Equatorial Guinea (French)",
			"locale" : "gq/fr.html?icid=site_selector_gq",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Eesti (Eesti)",
			"memberFirmLanguageTranslation" : "Estonia (Estonian)",
			"locale" : "ee/et.html?icid=site_selector_ee",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Estonia (English)",
			"memberFirmLanguageTranslation" : "Estonia (English)",
			"locale" : "ee/en.html?icid=site_selector_ee",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Ethiopia (English)",
			"memberFirmLanguageTranslation" : "Ethiopia (English)",
			"locale" : "et/en.html?icid=site_selector_et",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Finland (English)",
			"memberFirmLanguageTranslation" : "Finland (English)",
			"locale" : "fi/en.html?icid=site_selector_fi",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Suomi (Suomalainen)",
			"memberFirmLanguageTranslation" : "Finland (Finnish)",
			"locale" : "fi/fi.html?icid=site_selector_fi",
			"activateStatus" : "false"
		}, {
			"localeLanguageTranslation" : "France (Français)",
			"memberFirmLanguageTranslation" : "France (French)",
			"locale" : "fr/fr.html?icid=site_selector_fr",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Gabon (Français)",
			"memberFirmLanguageTranslation" : "Gabon (French)",
			"locale" : "ga/fr.html?icid=site_selector_ga",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Georgia (English)",
			"memberFirmLanguageTranslation" : "Georgia (English)",
			"locale" : "ge/en.html?icid=site_selector_ge",
			"redirectLink" : "http://www2.deloitte.com/ge/en.html?icid=site_selector_ge",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "საქართველო (ქართული)",
			"memberFirmLanguageTranslation" : "Georgia (Georgian)",
			"locale" : "ge/ka.html?icid=site_selector_ge",
			"redirectLink" : "http://www2.deloitte.com/ge/en.html?icid=site_selector_ge",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Deutschland (Deutsch)",
			"memberFirmLanguageTranslation" : "Germany (German)",
			"locale" : "de/de.html?icid=site_selector_de",
			"activateStatus" : "true"
						
		}, {
			"localeLanguageTranslation" : "Ghana (English)",
			"memberFirmLanguageTranslation" : "Ghana (English)",
			"locale" : "gh/en.html?icid=site_selector_gh",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Gibraltar (English)",
			"memberFirmLanguageTranslation" : "Gibraltar (English)",
			"locale" : "gi/en.html?icid=site_selector_gi",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Greece (English)",
			"memberFirmLanguageTranslation" : "Greece (English)",
			"locale" : "gr/en.html?icid=site_selector_gr",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Guam (English)",
			"memberFirmLanguageTranslation" : "Guam (English)",
			"locale" : "gu/en.html?icid=site_selector_gu",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Guatemala (Español)",
			"memberFirmLanguageTranslation" : "Guatemala (Spanish)",
			"locale" : "gt/es.html?icid=site_selector_gt",
			"redirectLink" : "http://www2.deloitte.com/gt/es.html?icid=site_selector_gt",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Guatemala (English)",
			"memberFirmLanguageTranslation" : "Guatemala (English)",
			"locale" : "gt/en.html?icid=site_selector_gt",
			"redirectLink" : "http://www2.deloitte.com/gt/es.html?icid=site_selector_gt",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Honduras (Español)",
			"memberFirmLanguageTranslation" : "Honduras (Spanish)",
			"locale" : "hn/es.html?icid=site_selector_hn",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Hong Kong (Chinese)",
			"memberFirmLanguageTranslation" : "Hong Kong (Chinese)",
			"locale" : "hk/zh.html?icid=site_selector_hk",
			"redirectLink" : "http://www2.deloitte.com/cn/zh.html?icid=site_selector_hk",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Hong Kong (English)",
			"memberFirmLanguageTranslation" : "Hong Kong (English)",
			"locale" : "hk/en.html?icid=site_selector_hk",
			"redirectLink" : "http://www2.deloitte.com/cn/en.html?icid=site_selector_hk",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Magyarország (Magyar)",
			"memberFirmLanguageTranslation" : "Hungary (Hungarian)",
			"locale" : "hu/hu.html?icid=site_selector_hu",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Hungary (English)",
			"memberFirmLanguageTranslation" : "Hungary (English)",
			"locale" : "hu/en.html?icid=site_selector_hu",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Iceland (Icelandic)",
			"memberFirmLanguageTranslation" : "Iceland (Icelandic)",
			"locale" : "is/is.html?icid=site_selector_is",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "India (English)",
			"memberFirmLanguageTranslation" : "India (English)",
			"locale" : "in/en.html?icid=site_selector_in",
			"activateStatus" : "true",
			"abbr" : "IND"
		}, {
			"localeLanguageTranslation" : "India (Offices of the US) (English)",
			"memberFirmLanguageTranslation" : "India (Offices of the US) (English)",
			"locale" : "ui/en.html?icid=site_selector_ui",
			"activateStatus" : "true",
			"abbr" : "IND"
		}, {
			"localeLanguageTranslation" : "Indonesia (English)",
			"memberFirmLanguageTranslation" : "Indonesia (English)",
			"locale" : "id/en.html?icid=site_selector_id",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Iraq (English)",
			"memberFirmLanguageTranslation" : "Iraq (English)",
			"locale" : "iq/en.html?icid=site_selector_iq",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Ireland (English)",
			"memberFirmLanguageTranslation" : "Ireland (English)",
			"locale" : "ie/en.html?icid=site_selector_ie",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Islamic Finance (English)",
			"memberFirmLanguageTranslation" : "Islamic Finance (English)",
			"locale" : "xd/en.html?icid=site_selector_xd",
			"redirectLink" : " http://www2.deloitte.com/lu/en/pages/islamic-finance/topics/islamic-finance.html?icid=site_selector_xd",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Israel (English)",
			"memberFirmLanguageTranslation" : "Israel (English)",
			"locale" : "il/en.html?icid=site_selector_il",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Italia (Italiano)",
			"memberFirmLanguageTranslation" : "Italy (Italian)",
			"locale" : "it/it.html?icid=site_selector_it",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Côte d'Ivoire (Français)",
			"memberFirmLanguageTranslation" : "Ivory Coast (French)",
			"locale" : "ci/fr.html?icid=site_selector_ci",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "日本 (日本語)",
			"memberFirmLanguageTranslation" : "Japan (Japanese)",
			"locale" : "jp/ja.html?icid=site_selector_jp",
			"redirectLink" : "/content/www/jp/ja.html?icid=site_selector_jp",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Japan (English)",
			"memberFirmLanguageTranslation" : "Japan (English)",
			"locale" : "jp/en.html?icid=site_selector_jp",
			"redirectLink" : "/content/www/jp/ja.html?icid=site_selector_jp",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Jordan (English)",
			"memberFirmLanguageTranslation" : "Jordan (English)",
			"locale" : "jo/en.html?icid=site_selector_jo",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Казахстан (Русский)",
			"memberFirmLanguageTranslation" : "Kazakhstan (Russian)",
			"locale" : "kz/ru.html?icid=site_selector_kz",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Kazakhstan (English)",
			"memberFirmLanguageTranslation" : "Kazakhstan (English)",
			"locale" : "kz/en.html?icid=site_selector_kz",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Kenya (English)",
			"memberFirmLanguageTranslation" : "Kenya (English)",
			"locale" : "ke/en.html?icid=site_selector_ke",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "대한민국 (한국어)",
			"memberFirmLanguageTranslation" : "Korea (Korean)",
			"locale" : "kr/ko.html?icid=site_selector_kr",
			"redirectLink" : "http://www2.deloitte.com/kr/ko.html?icid=site_selector_kr",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Korea (English)",
			"memberFirmLanguageTranslation" : "Korea (English)",
			"locale" : "kr/en.html?icid=site_selector_kr",
			"redirectLink" : "http://www2.deloitte.com/kr/ko.html?icid=site_selector_kr",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Kuwait (English)",
			"memberFirmLanguageTranslation" : "Kuwait (English)",
			"locale" : "kw/en.html?icid=site_selector_kw",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Кыргызстан (Русский)",
			"memberFirmLanguageTranslation" : "Kyrgyzstan (Russian)",
			"locale" : "kg/ru.html?icid=site_selector_kg",
			"redirectLink" : "http://www2.deloitte.com/kz/ru/pages/kyrgyzstan/topics/kyrgyzstan-practice.html?icid=site_selector_kg",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Kyrgyzstan (English)",
			"memberFirmLanguageTranslation" : "Kyrgyzstan (English)",
			"locale" : "kg/en.html?icid=site_selector_kg",
			"redirectLink" : "www2.deloitte.com/kz/ru/pages/kyrgyzstan/topics/kyrgyzstan-practice.html?icid=site_selector_kg",
			"activateStatus" : "false"
		}, {
			"localeLanguageTranslation" : "Lao PDR (English)",
			"memberFirmLanguageTranslation" : "Lao PDR (English)",
			"locale" : "la/en.html?icid=site_selector_la",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Latvia (English)",
			"memberFirmLanguageTranslation" : "Latvia (English)",
			"locale" : "lv/en.html?icid=site_selector_lv",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Lebanon (English)",
			"memberFirmLanguageTranslation" : "Lebanon (English)",
			"locale" : "lb/en.html?icid=site_selector_lb",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Libya (English)",
			"memberFirmLanguageTranslation" : "Libya (English)",
			"locale" : "ly/en.html?icid=site_selector_ly",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Liechtenstein (German)",
			"memberFirmLanguageTranslation" : "Liechtenstein (German)",
			"locale" : "li/de.html?icid=site_selector_li",
			"redirectLink" : "http://www2.deloitte.com/ch/de.html?icid=site_selector_li",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Lietuva (Lietuvos)",
			"memberFirmLanguageTranslation" : "Lithuania (Lithuanian)",
			"locale" : "lt/lt.html?icid=site_selector_lt",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Lithuania (English)",
			"memberFirmLanguageTranslation" : "Lithuania (English)",
			"locale" : "lt/en.html?icid=site_selector_lt",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Luxembourg (English)",
			"memberFirmLanguageTranslation" : "Luxembourg (English)",
			"locale" : "lu/en.html?icid=site_selector_lu",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Macau (Chinese)",
			"memberFirmLanguageTranslation" : "Macau (Chinese)",
			"locale" : "mo/zh.html?icid=site_selector_mo",
			"redirectLink" : "http://www2.deloitte.com/cn/zh.html?icid=site_selector_mo",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Macau (English)",
			"memberFirmLanguageTranslation" : "Macau (English)",
			"locale" : "mo/en.html?icid=site_selector_mo",
			"redirectLink" : "http://www2.deloitte.com/cn/en.html?icid=site_selector_mo",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Macedonia (English)",
			"memberFirmLanguageTranslation" : "Macedonia (English)",
			"locale" : "mk/en.html?icid=site_selector_mk",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Malaysia (English)",
			"memberFirmLanguageTranslation" : "Malaysia (English)",
			"locale" : "my/en.html?icid=site_selector_my",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Malta (English)",
			"memberFirmLanguageTranslation" : "Malta (English)",
			"locale" : "mt/en.html?icid=site_selector_mt",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Mauritius (English)",
			"memberFirmLanguageTranslation" : "Mauritius (English)",
			"locale" : "mu/en.html?icid=site_selector_mu",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "México (Español)",
			"memberFirmLanguageTranslation" : "Mexico (Spanish)",
			"locale" : "mx/es.html?icid=site_selector_mx",
			"redirectLink" : "http://www2.deloitte.com/mx/es.html?icid=site_selector_mx",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Middle East (English)",
			"memberFirmLanguageTranslation" : "Middle East (English)",
			"locale" : "xe/en.html?icid=site_selector_xe",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Moldova (Română)",
			"memberFirmLanguageTranslation" : "Moldova (Romanian)",
			"locale" : "md/ro.html?icid=site_selector_md",
			"redirectLink" : "http://www2.deloitte.com/ro/ro.html?icid=site_selector_md",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Mongolia (English)",
			"memberFirmLanguageTranslation" : "Mongolia (English)",
			"locale" : "mn/en.html?icid=site_selector_mn",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Montenegro (English)",
			"memberFirmLanguageTranslation" : "Montenegro (English)",
			"locale" : "me/en.html?icid=site_selector_me",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Maroc (Français)",
			"memberFirmLanguageTranslation" : "Morocco (French)",
			"locale" : "ma/fr.html?icid=site_selector_ma",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Myanmar (English)",
			"memberFirmLanguageTranslation" : "Myanmar (English)",
			"locale" : "mm/en.html?icid=site_selector_mm",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Namibia (English)",
			"memberFirmLanguageTranslation" : "Namibia (English)",
			"locale" : "na/en.html?icid=site_selector_na",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Nederland (Nederlands)",
			"memberFirmLanguageTranslation" : "Netherlands (Dutch)",
			"locale" : "nl/nl.html?icid=site_selector_nl",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "New Zealand (English)",
			"memberFirmLanguageTranslation" : "New Zealand (English)",
			"locale" : "nz/en.html?icid=site_selector_nz",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Nicaragua (Español)",
			"memberFirmLanguageTranslation" : "Nicaragua (Spanish)",
			"locale" : "ni/es.html?icid=site_selector_ni",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Nigeria (English)",
			"memberFirmLanguageTranslation" : "Nigeria (English)",
			"locale" : "ng/en.html?icid=site_selector_ng",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Norge (Norsk)",
			"memberFirmLanguageTranslation" : "Norway (Norwegian)",
			"locale" : "no/no.html?icid=site_selector_no",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Oman (English)",
			"memberFirmLanguageTranslation" : "Oman (English)",
			"locale" : "om/en.html?icid=site_selector_om",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Pakistan (English)",
			"memberFirmLanguageTranslation" : "Pakistan (English)",
			"locale" : "pk/en.html?icid=site_selector_pk",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Palestinian Ruled Territories (English)",
			"memberFirmLanguageTranslation" : "Palestinian Ruled Territories (English)",
			"locale" : "gz/en.html?icid=site_selector_gz",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Panamá (Español)",
			"memberFirmLanguageTranslation" : "Panama (Spanish)",
			"locale" : "pa/es.html?icid=site_selector_pa",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Papua New Guinea (English)",
			"memberFirmLanguageTranslation" : "Papua New Guinea (English)",
			"locale" : "pg/en.html?icid=site_selector_pg",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Paraguay (Español)",
			"memberFirmLanguageTranslation" : "Paraguay (Spanish)",
			"locale" : "py/es.html?icid=site_selector_py",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Perú (Español)",
			"memberFirmLanguageTranslation" : "Peru (Spanish)",
			"locale" : "pe/es.html?icid=site_selector_pe",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Philippines (English)",
			"memberFirmLanguageTranslation" : "Philippines (English)",
			"locale" : "ph/en.html?icid=site_selector_ph",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Polska (Polski)",
			"memberFirmLanguageTranslation" : "Poland (Polish)",
			"locale" : "pl/pl.html?icid=site_selector_pl",
			"redirectLink" : "http://www2.deloitte.com/pl/pl.html?icid=site_selector_pl",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Poland (English)",
			"memberFirmLanguageTranslation" : "Poland (English)",
			"locale" : "pl/en.html?icid=site_selector_pl",
			"redirectLink" : "http://www2.deloitte.com/pl/pl.html?icid=site_selector_pl",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Portugal (Português)",
			"memberFirmLanguageTranslation" : "Portugal (Portuguese)",
			"locale" : "pt/pt.html?icid=site_selector_pt",
			"redirectLink" : "http://www2.deloitte.com/pt/pt.html?icid=site_selector_pt",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Portugal (English)",
			"memberFirmLanguageTranslation" : "Portugal (English)",
			"locale" : "pt/en.html?icid=site_selector_pt",
			"redirectLink" : "http://www2.deloitte.com/pt/pt.html?icid=site_selector_pt",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Qatar (English)",
			"memberFirmLanguageTranslation" : "Qatar (English)",
			"locale" : "qa/en.html?icid=site_selector_qa",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Romania (Română)",
			"memberFirmLanguageTranslation" : "Romania (Romanian)",
			"locale" : "ro/ro.html?icid=site_selector_ro",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Romania (English)",
			"memberFirmLanguageTranslation" : "Romania (English)",
			"locale" : "ro/en.html?icid=site_selector_ro",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Россия (Русский)",
			"memberFirmLanguageTranslation" : "Russia (Russian)",
			"locale" : "ru/ru.html?icid=site_selector_ru",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Russia (English)",
			"memberFirmLanguageTranslation" : "Russia (English)",
			"locale" : "ru/en.html?icid=site_selector_ru",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Rwanda (English)",
			"memberFirmLanguageTranslation" : "Rwanda (English)",
			"locale" : "rw/en.html?icid=site_selector_rw",
			"activateStatus" : "false"
		}, {
			"localeLanguageTranslation" : "Saudi Arabia (English)",
			"memberFirmLanguageTranslation" : "Saudi Arabia (English)",
			"locale" : "sa/en.html?icid=site_selector_sa",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Sénégal (Français)",
			"memberFirmLanguageTranslation" : "Senegal (French)",
			"locale" : "sn/fr.html?icid=site_selector_sn",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Србија (Српски)",
			"memberFirmLanguageTranslation" : "Serbia (Serbian)",
			"locale" : "rs/sr.html?icid=site_selector_rs",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Serbia (English)",
			"memberFirmLanguageTranslation" : "Serbia (English)",
			"locale" : "rs/en.html?icid=site_selector_rs",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Singapore (English)",
			"memberFirmLanguageTranslation" : "Singapore (English)",
			"locale" : "sg/en.html?icid=site_selector_sg",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Slovenská republika (Slovenčina)",
			"memberFirmLanguageTranslation" : "Slovak Republic (Slovak)",
			"locale" : "sk/sk.html?icid=site_selector_sk",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Slovak Republic (English)",
			"memberFirmLanguageTranslation" : "Slovak Republic (English)",
			"locale" : "sk/en.html?icid=site_selector_sk",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Slovenija (Slovenščina)",
			"memberFirmLanguageTranslation" : "Slovenia (Slovenian)",
			"locale" : "si/sl.html?icid=site_selector_si",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Slovenia (English)",
			"memberFirmLanguageTranslation" : "Slovenia (English)",
			"locale" : "si/en.html?icid=site_selector_si",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "South Africa (English)",
			"memberFirmLanguageTranslation" : "South Africa (English)",
			"locale" : "za/en.html?icid=site_selector_za",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "España (Español)",
			"memberFirmLanguageTranslation" : "Spain (Spanish)",
			"locale" : "es/es.html?icid=site_selector_es",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Sverige (Svenska)",
			"memberFirmLanguageTranslation" : "Sweden (Swedish)",
			"locale" : "se/sv.html?icid=site_selector_se",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Switzerland (English)",
			"memberFirmLanguageTranslation" : "Switzerland (English)",
			"locale" : "ch/en.html?icid=site_selector_ch",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Schweiz (Deutsch)",
			"memberFirmLanguageTranslation" : "Switzerland (German)",
			"locale" : "ch/de.html?icid=site_selector_ch",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Suisse (Français)",
			"memberFirmLanguageTranslation" : "Switzerland (French)",
			"locale" : "ch/fr.html?icid=site_selector_ch",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Syria (English)",
			"memberFirmLanguageTranslation" : "Syria (English)",
			"locale" : "sy/en.html?icid=site_selector_sy",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "台灣 (中國)",
			"memberFirmLanguageTranslation" : "Taiwan (Chinese)",
			"locale" : "tw/tc.html?icid=site_selector_tw",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Taiwan (English)",
			"memberFirmLanguageTranslation" : "Taiwan (English)",
			"locale" : "tw/en.html?icid=site_selector_tw",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Таджикистан (Русский)",
			"memberFirmLanguageTranslation" : "Tajikistan (Russian)",
			"locale" : "tj/ru.html?icid=site_selector_tj",
			"redirectLink" : "http://www2.deloitte.com/kz/ru/pages/tajikistan/topics/tajikistan.html?icid=site_selector_tj",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Tajikistan (English)",
			"memberFirmLanguageTranslation" : "Tajikistan (English)",
			"locale" : "tj/en.html?icid=site_selector_tj",
			"activateStatus" : "false"
		}, {
			"localeLanguageTranslation" : "Tanzania (English)",
			"memberFirmLanguageTranslation" : "Tanzania (English)",
			"locale" : "tz/en.html?icid=site_selector_tz",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Thailand (English)",
			"memberFirmLanguageTranslation" : "Thailand (English)",
			"locale" : "th/en.html?icid=site_selector_th",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Timor-Leste (English)",
			"memberFirmLanguageTranslation" : "Timor-Leste (English)",
			"locale" : "tl/en.html?icid=site_selector_tl",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Togo (Français)",
			"memberFirmLanguageTranslation" : "Togo (French)",
			"locale" : "tg/fr.html?icid=site_selector_tg",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Trinidad and Tobago (English)",
			"memberFirmLanguageTranslation" : "Trinidad and Tobago (English)",
			"locale" : "tt/en.html?icid=site_selector_tt",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Tunisie (Français)",
			"memberFirmLanguageTranslation" : "Tunisia (French)",
			"locale" : "tn/fr.html?icid=site_selector_tn",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Türkiye (Türk)",
			"memberFirmLanguageTranslation" : "Turkey (Turkish)",
			"locale" : "tr/tr.html?icid=site_selector_tr",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Turkey (English)",
			"memberFirmLanguageTranslation" : "Turkey (English)",
			"locale" : "tr/en.html?icid=site_selector_tr",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "US - Corporate Finance (English)",
			"memberFirmLanguageTranslation" : "US - Corporate Finance (English)",
			"locale" : "xa/en.html?icid=site_selector_xa",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Uganda (English)",
			"memberFirmLanguageTranslation" : "Uganda (English)",
			"locale" : "ug/en.html?icid=site_selector_ug",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Україна (Український)",
			"memberFirmLanguageTranslation" : "Ukraine (Ukrainian)",
			"locale" : "ua/uk.html?icid=site_selector_ua",
			"redirectLink" : "http://www2.deloitte.com/ua/uk.html?icid=site_selector_ua",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Ukraine (English)",
			"memberFirmLanguageTranslation" : "Ukraine (English)",
			"locale" : "ua/en.html?icid=site_selector_ua",
			"redirectLink" : "http://www2.deloitte.com/ua/uk.html?icid=site_selector_ua",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "United Arab Emirates (English)",
			"memberFirmLanguageTranslation" : "United Arab Emirates (English)",
			"locale" : "ae/en.html?icid=site_selector_ae",
			"activateStatus" : "true",
			"abbr" : "UAE"
		}, {
			"localeLanguageTranslation" : "United Kingdom (English)",
			"memberFirmLanguageTranslation" : "United Kingdom (English)",
			"locale" : "uk/en.html?icid=site_selector_uk",
			"redirectLink" : "http://www2.deloitte.com/uk/en.html?icid=site_selector_uk",
			"activateStatus" : "true",
			"abbr" : "UK"
		}, {
			"localeLanguageTranslation" : "United States (English)",
			"memberFirmLanguageTranslation" : "United States (English)",
			"locale" : "us/en.html?icid=site_selector_us",
			"activateStatus" : "true",
			"abbr" :"USA"
			
		}, {
			"localeLanguageTranslation" : "United States Virgin Islands (English)",
			"memberFirmLanguageTranslation" : "United States Virgin Islands (English)",
			"locale" : "vi/en.html?icid=site_selector_vi",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Uruguay (Español)",
			"memberFirmLanguageTranslation" : "Uruguay (Spanish)",
			"locale" : "uy/es.html?icid=site_selector_uy",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Узбекистан (Русский)",
			"memberFirmLanguageTranslation" : "Uzbekistan (Russian)",
			"locale" : "uz/ru.html?icid=site_selector_uz",
			"redirectLink" : "http://www2.deloitte.com/kz/ru/pages/uzbekistan/topics/uzbekistan.html?icid=site_selector_uz",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Uzbekistan (English)",
			"memberFirmLanguageTranslation" : "Uzbekistan (English)",
			"locale" : "uz/en.html?icid=site_selector_uz",
			"activateStatus" : "false"
		}, {
			"localeLanguageTranslation" : "Venezuela (Español)",
			"memberFirmLanguageTranslation" : "Venezuela (Spanish)",
			"locale" : "ve/es.html?icid=site_selector_ve",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Vietnam (English)",
			"memberFirmLanguageTranslation" : "Vietnam (English)",
			"locale" : "vn/en.html?icid=site_selector_vn",
			"activateStatus" : "true"
		}, {
			"localeLanguageTranslation" : "Yemen (English)",
			"memberFirmLanguageTranslation" : "Yemen (English)",
			"locale" : "ye/en.html?icid=site_selector_ye",
			"activateStatus" : "true"
		}
	],
	"count" : [{
			"sitesAuthor" : "Available in 148 locations and 35 languages",
			"sitesPublish" : "Available in 145 locations and 34 languages"
		}
	],
	"aemMode" : "DISABLED",
	"createdAt" : 1452766851799
}
