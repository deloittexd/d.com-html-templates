
 var cookieHeight = $(".cookie-container").outerHeight(true);
 var headerHeight = $(".header-container").outerHeight(true);
 var breadcrumbHeight = $(".breadcrumb-section-bar").outerHeight(true);
 var positionTop = cookieHeight + headerHeight + breadcrumbHeight;
 var scrollBarHeight=$('.scrollContainer').outerHeight();
 var rightSectionHeight=$('.content-page-info .right-content-section').height();
 var rightSectionWidth=$('.content-page-info .right-content-section').outerWidth();
 var rightSectionTop=positionTop+scrollBarHeight;

/* Code for progress bar */
$(document).ready(function(){            
    $('.wrapper').progressScroll();
})
/* Code for progress bar ends */

/* Next Step Component code */
contactListClick=function(obj,event){
	var ht=0;

	if($(obj).attr("href")=="#"){
		event.preventDefault();
		if(!$(obj).parent().hasClass("selected")){
			$(obj).parent().addClass("selected");
			//$(obj).parent().find(".overlay .pop-up").css("","");
			if (matchMedia('(min-width: 768px)').matches){
				$(obj).parent().prevAll().each(function(){
					ht+=$(this).height();
				});
				if(ht==0){
					ht+=20;
				}
				ht+=$(obj).parent().height();
				ht=-Math.abs(ht);
				$(obj).parent().find(".overlay-set .pop-up").css("margin-top",ht);
				$(obj).parent().find(".overlay-set").removeClass("hide-it");

				//console.log("before print ht");
                overlayHeight=$(obj).parent().find('.overlay-set .pop-up').height();
                windowInnerHt=$(window).innerHeight();
				remAvailableWindow=windowInnerHt-rightSectionTop-30;//adjusting padding top
              	//console.log("pop up ht: "+overlayHeight);
				//console.log("window ht :"+$(window).innerHeight());
                //console.log("rt ht"+rightSectionTop);
                //console.log("window ht availableht"+remAvailableWindow);
				if(overlayHeight>remAvailableWindow){
                   // console.log("enetred avail ht less");

					$(obj).parent().find(".overlay-set .pop-up").css({"max-height":remAvailableWindow,"overflow-y":"scroll"});
				}

				$(obj).parent().find(".overlay-set .pop-up").animate({"right": "100%"},150);
			}
			else{
				setTimeout(function(){$(obj).parent().find(".overlay-set").removeClass("hide-it");
				$(obj).parent().find(".overlay-set .pop-up").slideDown("slow"); }, 200);
			}
			$('.poll').css("position","static");
			$('.poll .icon-survey').css("visibility","hidden");
			$('#footer-section').css("position","static");
			$('#footer-section .footer-wrapper .footer-container').css("position","static");
			$('.tag-panel .article-tags li a').css("position","static");
		}
		else{
			if (matchMedia('(min-width: 768px)').matches){
				$(obj).parent().find(".overlay-set .pop-up").animate({"right": "6%"},150,function(){
					$(obj).parent().find(".overlay-set").addClass("hide-it"); 
					$(obj).parent().removeClass("selected");
                    $('.poll').css("position","relative");
					$('.poll .icon-survey').css("visibility","visible");
					$('#footer-section').css("position","relative");
					$('#footer-section .footer-wrapper .footer-container').css("position","relative");
					$('.tag-panel .article-tags li a').css("position","relative");
				});
			}
			else{
				$(obj).parent().find(".overlay-set").addClass("hide-it"); 
				$(obj).parent().removeClass("selected");
                $('.poll').css("position","relative");
				$('.poll .icon-survey').css("visibility","visible");
				$('#footer-section').css("position","relative");
				$('#footer-section .footer-wrapper .footer-container').css("position","relative");
				$('.tag-panel .article-tags li a').css("position","relative");
			}

		}
	}
}
modalClick=function(obj,event){
	if (matchMedia('(min-width: 768px)').matches){
		$(obj).parent().find('.pop-up').animate({"right": "6%"},150,function(){
			$(obj).parent().addClass("hide-it");
			$(obj).closest('li.contact-list-item').removeClass("selected");
            $('.poll').css("position","relative");
			$('.poll .icon-survey').css("visibility","visible");
			$('#footer-section').css("position","relative");
			$('#footer-section .footer-wrapper .footer-container').css("position","relative");
			$('.tag-panel .article-tags li a').css("position","relative");
		});

	}
	else{
		$(obj).parent().addClass("hide-it");
		$(obj).closest('li.contact-list-item').removeClass("selected");
        $('.poll').css("position","relative");
		$('.poll .icon-survey').css("visibility","visible");
		$('#footer-section').css("position","relative");
		$('#footer-section .footer-wrapper .footer-container').css("position","relative");
		$('.tag-panel .article-tags li a').css("position","relative");
		$('html,body').animate({ scrollTop:$(obj).closest('li.contact-list-item').offset().top},'slow');
	}

}
closeButtonClick=function(obj,event){
	if (matchMedia('(min-width: 768px)').matches){
		$(obj).parent().animate({"right": "6%"},150,function(){
			$(obj).closest('div.overlay-set').addClass("hide-it");
			$(obj).closest('li.contact-list-item').removeClass("selected");
            $('.poll').css("position","relative");
			$('.poll .icon-survey').css("visibility","visible");
			$('#footer-section').css("position","relative");
			$('#footer-section .footer-wrapper .footer-container').css("position","relative");
			$('.tag-panel .article-tags li a').css("position","relative");
		});

	}
	else{
		$(obj).closest('div.overlay-set').addClass("hide-it");
		$(obj).closest('li.contact-list-item').removeClass("selected");
        $('.poll').css("position","relative");
		$('.poll .icon-survey').css("visibility","visible");
		$('#footer-section').css("position","relative");
		$('#footer-section .footer-wrapper .footer-container').css("position","relative");
		$('.tag-panel .article-tags li a').css("position","relative");
		$('html,body').animate({ scrollTop:$(obj).closest('li.contact-list-item').offset().top},'slow');
	}

	
}

/* Next Step code ends */
$(document).ready(function() {	
	/* code fix to handle anchor scroll issue in mobile*/
	if(matchMedia('(max-width: 767px)').matches) {
        var pathPage = window.location.href;
        if(pathPage.indexOf("#") > -1) {
          var anchorHash= pathPage.substring(pathPage.indexOf("#"), pathPage.length);
          $(".content-components").show();
          $(".button.read-more-btn").hide();
          var location = $("" +anchorHash+"").position().top;
          var headerHeight = $("#header").outerHeight(true);
          var location_other= location+headerHeight;
          $('html, body').stop().animate({scrollTop: location_other}, 500, 'swing');
        }
	}
	/* Function to handle read more in mobile */
    if(matchMedia('(max-width: 767px)').matches) {
		$(".button.read-more-btn").show();
    }
	positionNextSteps= $(".content-page-info .right-content-section").offset().left;
	$("#read-more").click(function(e) {
		e.preventDefault();
		/* US233353 increasing intial height of content in mobile */
        if(matchMedia('(max-width: 767px)').matches) {
		$(".wrapper.content-page .main-container.content-page .content.main .left-content-section").css("height","initial");
		}
		$(".button.read-more-btn").hide();
	});
	/* Function for read more ends */
	$(document).bind("keyup", null, function (e) {
			if (e.which == 27) {
                $('.contact-list .contact-list-item .overlay-set').each(function(){
                    var ob= $(this);
					$(ob).find('.pop-up').animate({"right": "6%"},150,function(){

						$(ob).addClass("hide-it"); 
						$(ob).closest('.contact-list-item').removeClass("selected");
						$('.poll').css("position","relative");
						$('.poll .icon-survey').css("visibility","visible");
						$('#footer-section').css("position","relative");
						$('#footer-section .footer-wrapper .footer-container').css("position","relative");
						$('.tag-panel .article-tags li a').css("position","relative");
					});
				});
			}
		});		
});


/* Scroll code for content page */
$(window).on('scroll', function () {
	 pageName = $(".main-container.content-page");
     cookieHeight = $(".cookie-container").outerHeight(true);
   	 headerHeight = $(".header-container").outerHeight(true);
     breadcrumbHeight = $(".breadcrumb-section-bar").outerHeight(true);
     positionTop = cookieHeight + headerHeight + breadcrumbHeight;
     scrollBarHeight=$('.scrollContainer').outerHeight();
     rightSectionHeight=$('.content-page-info .right-content-section').outerHeight();
     rightSectionTop=positionTop+scrollBarHeight;
	 if(pageName.length && matchMedia('(min-width: 768px)').matches){

	 	 heightScrolled = $(window).scrollTop();
	 	 if($(".content-page-info .share-bar").length) {
	 	 	heightFirstComp = $(".content-page-info .share-bar").position().top;
            heightFirstComp+=100;
	 	 }
	 	 else {
	 	 	heightFirstComp = $("#article-intro-post").position().top;
	 	 }
	 	 if($(".poll").length) {
	 	 	pollHeight = $(".poll").position().top;
	 	 }
	 	 else {
	 	 	pollHeight = $(".associated-articles").position().top;
	 	 }
		 $('.scrollWrapper .share-bar .social-share-links ul li:visible:last').css("margin-right","0px");


			
	 	if(heightScrolled > heightFirstComp && heightScrolled < pollHeight) {
	 		$(".scrollContainer").show().css("top",positionTop);
	 		//$(".main-container.content-page").css("margin-top","52px");
			if(heightScrolled < pollHeight-rightSectionHeight){
				$(".content-page-info .right-content-section").css({"position": "fixed","top" : rightSectionTop,"left" : positionNextSteps,"margin-right":"0px","width":rightSectionWidth,"margin-top":"-30px"}); 
				$(".content-page-info .right-content-section").addClass('right-scrolled');
				
	 		}
			else{
				$(".content-page-info .right-content-section").css({"position": "static"});
			}
		}
	 	else{
	 		$(".scrollContainer").hide();
	 		/*if($('.wrapper.content-page .main-container.content-page .header-intro-container .responsive-img-container').length!=0){
				$('.wrapper.content-page .main-container.content-page').css("margin-top","0px");
			}
			else
				$('.wrapper.content-page .main-container.content-page').css("margin-top","30px");	*/
	 		$(".content-page-info .right-content-section").css({"position":"relative","top":"","left":"","margin-right":"0px","width":"25%","margin-top":"0px"});
			$(".content-page-info .right-content-section").removeClass('right-scrolled');
	 		$('.scrollWrapper h4').html();
	 	}
	 }
	 else {
	 	var positionTop = cookieHeight + headerHeight;
	 	var heightScrolled = $(window).scrollTop();
	 	var heightFirstComp = $("#article-intro-post").position().top;
	 	if(heightScrolled > heightFirstComp) {
	 		$(".scrollContainer").show();
	 	}
	 	else{
	 		$(".scrollContainer").hide();
	 	}
	 }
});
/* scroll code ends */
