
$( document ).ready(function() {
	var memberFirmSNPURL = 'https://sp10050c00.guided.ss-omtrdc.net/';
    var QueryName;
	
	var QueryString=$('.dynamic-lists-filters ul li:first').attr('data-snp-query');
	PageNumber= 1;
	NumOfResults=0;
    var TokenName;
	var JsonResponseList;
	var FinalQuery;
	var dynamicListPageJsonURL;
	var responseJson;
	var TemplateType;
	if($('.wrapper .main-container').hasClass('profiles-page')){
		TemplateType='profiles';
	}
	else{
		TemplateType='events';
	}
    if($('.wrapper .main-container.dynamic-list-page div').hasClass('no-facets')){
        QueryString=$('.wrapper .main-container.dynamic-list-page .no-facets').attr('data-snp-query');
    }
    else{
        QueryString=$('.dynamic-lists-filters ul li:first').attr('data-snp-query');
        $('.dynamic-lists-filters ul li:first').addClass('tabs-active state-active');
    }

	 /* Fucntion to render the json response */
	var renderJson = function(responseJson) {
        
        $(".dynamic-list-items").html("");
		if(TemplateType=='events'){
            $.each(responseJson['result-sets'], function(index, value) {    
                $(".dynamic-list-items").append("<li><div class='dynamic-list-image-container'><a href='"+value['url']+"'><img data-aspectratio='1:1' alt='image' src='' data-orgsrc="+value['image']+"/></a></div><div class='dynamic-list-text-container'><h2 class='tertiary-headline'><a class='g-results' href='"+value['url']+"'><span class='dynamic-list-headline1'>"+value['heading1']+"</span><span class='dynamic-list-headline2'>"+value['heading2']+"</span></a></h2><p>"+value['description']+"</p></div></li>");
            });
        }
        else{
            $.each(responseJson['result-sets'], function(index, value) {    
				if(value['employee-title']!="")
					$(".dynamic-list-items").append("<li><div class='dynamic-list-image-container'><a href="+value['url']+"><img data-aspectratio='1:1' alt='image' src='' data-orgsrc="+value['image']+"></a></div><div class='dynamic-list-text-container'><h2 class='tertiary-headline'><a class='g-results' href="+value['url']+">"+value['name']+" | "+value['employee-title']+"</a></h2><p>"+value['description']+"</p></div></li>");
				else
					$(".dynamic-list-items").append("<li><div class='dynamic-list-image-container'><a href="+value['url']+"><img data-aspectratio='1:1' alt='image' src='' data-orgsrc="+value['image']+"></a></div><div class='dynamic-list-text-container'><h2 class='tertiary-headline'><a class='g-results' href="+value['url']+">"+value['name']+"</a></h2><p>"+value['description']+"</p></div></li>");
            });
        }
		setAllImgsRendition();
		NumOfResults=JsonResponseList['total-results'];
		if(JsonResponseList['total-results']>100){
			NumOfResults=100;
		}
		
		if($('.results-count').text()==""){
			$('.results-count').text(resultsShowI18nVal+" "+NumOfResults+" "+pageTypeI18nVal);
		}
		if(responseJson['total-results']!=0&&responseJson['total-results']>10){
            doPagination();
        }


	};

    /* Function to fetch json value */
    var getData = function() {
        //QueryName = x;
        FinalQuery= QueryString+'&page='+PageNumber;
        dynamicListPageJsonURL= memberFirmSNPURL+FinalQuery;
		$.ajax({
            url: dynamicListPageJsonURL,
            dataType: "jsonp",
			success: function (responseJson) {
				JsonResponseList=responseJson;
                renderJson(responseJson);
            },
			error: function(){
				console.log("snp did not provide a response");
			}
        })
    };

    /* function to call different page no */
    var doPagination = function() {
		if($('.pagination ul li').length==0){
			var max_pages=JsonResponseList['total-pages'];
			if(max_pages>10){
				max_pages=10;
			}
			$('.pagination ul').append('<li class="page-number button btn-blue btn-white" onclick="pageNumberClick(this)" data-page="true"><a href="#"><span class="audible">'+searchCurrentPage+'</span><span>1</span></a></li>');
			for(var i=2;i<=max_pages;i++){
				$('.pagination ul').append('<li class="page-number button btn-white" onclick="pageNumberClick(this)" data-page="false"><a href="#"><span class="audible">'+searchViewPage+'</span><span>'+i+'</span></a></li>');
			}
			$('.pagination').css('border-top','1px solid #d7dbdb');
		}

    };
	facetClick= function(obj){
		if($(obj).parent().attr('data-selected')!="true"){
			$('.results-count').text("");
			$(obj).addClass("btn-blue").removeClass("btn-white");
			$(obj).parent().attr({'aria-selected':'true','aria-expanded':'true','data-selected':'true'}).addClass('tabs-active state-active');
			$(obj).parent().siblings().attr({'aria-selected':'false','aria-expanded':'false','data-selected':'false'}).removeClass('tabs-active state-active');
			$(obj).parent().siblings().find('a').addClass('btn-white').removeClass('btn-blue');
			$('.pagination ul').html('');
			PageNumber=1;
			QueryString=$(obj).parent().attr('data-snp-query');
			getData();
				
		}
	}
	pageNumberClick= function(obj){
		if($(obj).attr('data-page')=='false'){
			PageNumber=$(obj).find('a span:last').text();
			$(obj).attr('data-page','true').find('a span:first').text(searchCurrentPage);
			$(obj).siblings().attr('data-page','false').find('a span:first').text(searchViewPage);
			$(obj).addClass('btn-blue');
			$(obj).siblings().removeClass('btn-blue');
			getData();
			$("html, body").animate({ scrollTop: 0 }, "slow");
		}
	};
	
	getData();
    if (matchMedia('(max-width: 767px)').matches) {
         $(".dynamic-lists-filters").addOverflow();
    } 

});

