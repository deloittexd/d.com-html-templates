var playerCurrentlyPlaying = null;
var videoName = "";
var templateName = "";
var pageName = "";
var youtubeVideoId = "";
var pageType = "";
var bluprntPath = "";
var youtubeplayers = $('.youtubevars');
if (youtubeplayers.length > 0) {
   $dcom.scriptLoadAndCache("https://www.youtube.com/player_api" )
			.done(function( script, textStatus ) {
                console.log('YouTube API Loaded and Ready');
			})
			.fail(function( jqxhr, settings, exception ) {
                console.log('Unable to load YouTube API', exception);
			});

	}
  var player = new Array();
 function onYouTubePlayerAPIReady() {

    if(youtubeplayers.length>0){

        for(var i=0; i < youtubeplayers.length; i++){

            var divID = $(youtubeplayers[i]).attr('id');

            var videoID = $(youtubeplayers[i]).attr('data-videoid');

            var playList = $(youtubeplayers[i]).attr('data-playList');

            templateName = $(youtubeplayers[i]).attr('data-templateName');

            pageName = $(youtubeplayers[i]).attr('data-pageName');

            var playListID = $(youtubeplayers[i]).attr('data-playListID');

            var videoTitle = $(youtubeplayers[i]).attr('data-videoTitle');

            pageType = $(youtubeplayers[i]).attr('data-pageType');

            bluprntPath = $(youtubeplayers[i]).attr('data-blprntPath');

            player[i] = new YT.Player(divID, {
                width:'100%',
                height:'100%',
                videoId: videoID,
                title: videoTitle,
                playerVars: { 
                    'playlist': playList,
                    'rel': 0,
                    'cc_load_policy': 1,
                    'autohide': 0,
                    'wmode': "transparent",
                    'listType':'playlist',
         			'list': playListID

                },
                events: {
                    'onStateChange': function(event){

                        var mediaName=event.target.getVideoData().title;
                        var mediaLength = event.target.getDuration();
                        var mediaPlayerName = "Youtube Player";
                        var mediaOffset=0;


                        if (Math.floor(event.target.getCurrentTime()) > 0) {
                            mediaOffset = Math.floor(event.target.getCurrentTime());
                        } else {
                            mediaOffset = 0;
                        }

						
                        if (event.data == YT.PlayerState.PLAYING ) {
								console.log("playing");
								reportStart(mediaName,mediaLength,mediaPlayerName,mediaOffset);
                        }

                        if (event.data == YT.PlayerState.PAUSED) {

                            reportPause(mediaName,mediaOffset);                  
                        }

                        if (event.data == YT.PlayerState.BUFFERING) {

                            reportPause(mediaName,mediaOffset);                  
                        }

                        if (event.data == YT.PlayerState.ENDED) {

                            reportEnd(mediaName,mediaOffset);                  
                        }



                    }
                }
                
            });



        }
    }

}


function reportStart(mediaName,mediaLength,mediaPlayerName,mediaOffset){

    if (mediaOffset ==0) {
        s.Media.open(mediaName,mediaLength,mediaPlayerName);
        s.Media.play(mediaName,mediaOffset);
    } else {
        s.Media.play(mediaName,mediaOffset);
    }
}

function reportEnd(mediaName,mediaOffset){
    s.Media.stop(mediaName,mediaOffset);
    s.Media.close(mediaName);
}

function reportPause(mediaName,mediaOffset){
    s.Media.stop(mediaName,mediaOffset);
}

