(function($) {
    $(window).load(function(){                               
        var $tabSet = $(".dynamic-lists-filters");
        
        setTimeout(function(){
            $tabSet.addSwipe();
        },1000);

    });
	
    $.fn.addOverflow = function(){
        var $main = $(this);
        var $tabs = $main.find(".facet-button").parent();
        var total = $tabs.length;
        
        $main.addClass('overflow-tabs').css('width','100%');
        
        $tabs.wrapAll('<div class="tabbedMenu" />');
        $main.find('.tabbedMenu').wrap('<div class="tabHeader"/>');

        $main.css('height','40px')
            .css('margin-bottom','20px');
        $main.find('.tabbedMenu').taboverflow(); 
        $tabs.css('width','auto');
        var $w = 0;
        $tabs.each(function(){
        $w += $(this).outerWidth(true);
                $(this).width('auto');
        }); 
        $('.tabbedMenu').css({'width': $w + 100});

        $('.scrollArrows').css('display','block');
        
        var $activeBtn = $('.facet-button.btn-blue').last();
        $(".tabbedScrollWrap").animate({ scrollLeft: $activeBtn.position().left},10);

    }
    
    
    $.fn.addSwipe = function(){
                    
                var $main = $(this);
                var $tabs = $main.find(".facet-button").parent();
				
                var total = $tabs.length;    
                
                   
                var $swipeArea = $main;
                   
                //setup tab listeners
                
                var tabSwipeLeft = function(){
                    var $active = $main.find(".facet-button.btn-blue");
                    var $next = $active.prev();
                    
                    if($active.index() == 0)
                        $next = $tabs.eq(total-1);                 
                    
                    $next.trigger('click'); 
                }
                
                var tabSwipeRight = function(){
                    var $active = $main.find(".facet-button.btn-blue");
                    var $next = $active.next();
                    
                    if($active.index() == total-1)
                        $next = $tabs.eq(0);
                    
                    $next.trigger('click');  
                }
                
              
                $swipeArea.hammer().on("swiperight", tabSwipeLeft);
                $swipeArea.hammer().on("swipeleft", tabSwipeRight);
                
                
                return;

            };
    
    
})(jQuery);
