# README #

* Deloitte.com Vendor Code Repository Requirements and Expectations can be found here:
https://bitbucket.org/deloittexd/d.com-html-templates/src/master/vendor-code-repository-requirements-expectations.md

* Deloitte.com Custom HTML - Vendor Requirements for Design/Development can be found here:
https://bitbucket.org/deloittexd/d.com-html-templates/src/master/vendor-requirements-design-development.md

* Deloitte.com Custom HTML Agency SOW Language can be found here:
https://bitbucket.org/deloittexd/d.com-html-templates/src/master/agency-sow-language.md

* QA Browsing Matrix can be found here:
https://bitbucket.org/deloittexd/d.com-html-templates/src/master/QA-Browser-Testing-Matrix.xlsx

Please refer to both documents regarding the purpose of this repository and expectations on how it should be used.